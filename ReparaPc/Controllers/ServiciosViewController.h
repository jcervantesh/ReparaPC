//
//  ServiciosViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 28/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;

@interface ServiciosViewController : UIViewController



@property (strong, nonatomic) IBOutlet UITextField *txtServicio;
@property (strong, nonatomic) IBOutlet UITextField *txtCosto;
@property (strong, nonatomic) IBOutlet UIButton *btnGuardar;

@property (strong, nonatomic) FIRDatabaseReference *ref;

- (IBAction)back:(id)sender;
- (IBAction)guardar:(id)sender;


@end
