//
//  ServiciosViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 28/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ServiciosViewController.h"

#define Servicio             @"servicio"

@interface ServiciosViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *servicios;
}

@end

@implementation ServiciosViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _btnGuardar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btnGuardar.layer.borderWidth = 3.0f;
    _btnGuardar.layer.cornerRadius = 8.0f;
    [self SetTextFieldBorder:_txtServicio];
    [self SetTextFieldBorder:_txtCosto];
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        @try {
            servicios=[postDict objectForKey:@"servicios"];
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)back:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)guardar:(id)sender {
    NSString *serv;
    NSDictionary *post;
    long cont;
    long temp1=0;
    long temp2=0;
    
    cont=[servicios count];
    if(cont!=0){
        //####### Recorrer todos los registros
        NSArray * values = [servicios allValues];
        for(int i=0; i<=[values count]-1; i++){
            NSDictionary *aux=[values objectAtIndex:i];
            NSString *con=[aux objectForKey:@"id"];
            temp1=[con longLongValue];
            if(temp1>temp2){
                temp2=temp1;
            }
        }
        cont=temp2+1;
        
    }
    if(cont==0){
        cont=1;
    }
    serv=Servicio;
    serv=[serv stringByAppendingString:[NSString stringWithFormat: @"%ld", cont]];
    post= @{@"id":[NSString stringWithFormat: @"%ld", cont], @"servicio": _txtServicio.text,@"costo": _txtCosto.text};
    NSDictionary *childUpdates = @{[@"/servicios/" stringByAppendingString:serv]: post};
    
    [_ref updateChildValues:childUpdates];
}
-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

@end
