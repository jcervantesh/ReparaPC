//
//  ClientesActualizaEliminarViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 10/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@class ClientesConsultaViewController;
@interface ClientesActualizaEliminarViewController : UIViewController

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property(strong, nonatomic)ClientesConsultaViewController *clntConsultaController;

@property (strong, nonatomic) IBOutlet UITextField *txtNombre;
@property (strong, nonatomic) IBOutlet UITextField *txtaPaterno;
@property (strong, nonatomic) IBOutlet UITextField *txtaMaterno;
@property (strong, nonatomic) IBOutlet UITextField *txtTelefono;
@property (strong, nonatomic) IBOutlet UITextField *txtCiudad;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblTextoAlerta;


@property (strong, nonatomic) NSString *idCliente;

- (IBAction)back:(id)sender;
- (IBAction)actualizar:(id)sender;
- (IBAction)eliminar:(id)sender;
- (IBAction)confirmar:(id)sender;
- (IBAction)alertaccion:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnconfirmar;
@property (strong, nonatomic) IBOutlet UIButton *btneliminar;
@property (strong, nonatomic) IBOutlet UIButton *btnActualizar;
@property (strong, nonatomic) IBOutlet UIView *datosView;
@property (strong, nonatomic) IBOutlet UIView *transparentView;
@property (strong, nonatomic) IBOutlet UIView *eliminarView;
@property (strong, nonatomic) IBOutlet UIView *actualizarView;
@property (strong, nonatomic) IBOutlet UIView *confirmarView;


@end
