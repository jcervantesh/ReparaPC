//
//  ClientesEquiposViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 14/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ClientesEquiposViewController.h"
#import "EquiposRegistroViewController.h"
#import "EquiposConsultaViewController.h"
#import "TabBarController.h"
#import "StringKeys.h"

@interface ClientesEquiposViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *clientes;
    NSDictionary *aux;
}


@end

@implementation ClientesEquiposViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _tableView.layer.borderWidth = 2.0f;
    _tableView.layer.cornerRadius = 4.0f;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        clientes=[postDict objectForKey:@"usuarios"];
        [self.tableView reloadData];
    }];
    _equipregController=[[EquiposRegistroViewController alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [clientes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identificadorCelda=@"cell";
    UITableViewCell *celda=[tableView dequeueReusableCellWithIdentifier:identificadorCelda forIndexPath:indexPath];
    _values = [clientes allValues];
    aux=[_values objectAtIndex:[indexPath row]];
    NSString *name=[aux objectForKey:@"nombre"];
    NSString *mail=[aux objectForKey:@"email"];
    celda.textLabel.text=name;
    celda.detailTextLabel.text=mail;
    celda.layer.borderColor = [UIColor darkGrayColor].CGColor;
    celda.layer.borderWidth = 1.0f;
    return celda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    aux=[_values objectAtIndex:[indexPath row]];
    self.equipregController.idCliente = [aux objectForKey:@"id"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

-(void)recalculateTable{
    _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, ([clientes count]*50));
}
-(void) viewDidAppear:(BOOL)animated {
    NSLog(@"Actualizo datos");
    [self.tableView reloadData];
    [self recalculateTable];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.tabController = [segue destinationViewController];
    self.equipregController = [self.tabController.viewControllers objectAtIndex:0];
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
