//
//  EquiposRegistroViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 18/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "EquiposRegistroViewController.h"
#import "EquiposConsultaViewController.h"
#import "StringKeys.h"

@interface EquiposRegistroViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *equipos;
}

@end

@implementation EquiposRegistroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.delegate = self;
    [self SetTextFieldBorder:_txtmarca];
    [self SetTextFieldBorder:_txtequipo];
    [self SetTextFieldBorder:_txtpass];
    [self SetTextFieldBorder:_txtestado];
    [self SetTextFieldBorder:_txtobser];
    [self SetTextFieldBorder:_txtdesc];
    [self SetTextFieldBorder:_txtModelo];
    _btnGuardar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btnGuardar.layer.borderWidth = 3.0f;
    _btnGuardar.layer.cornerRadius = 8.0f;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        @try {
            equipos=[postDict objectForKey:@"equipos"];
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Guardar:(id)sender {
    NSString *equi;
    NSDictionary *post;
    long cont;
    long temp1=0;
    long temp2=0;
    
    cont=[equipos count];
    if(cont!=0){
        //####### Recorrer todos los registros
        NSArray * values = [equipos allValues];
        for(int i=0; i<=[values count]-1; i++){
            NSDictionary *aux=[values objectAtIndex:i];
            NSString *con=[aux objectForKey:@"id"];
            temp1=[con longLongValue];
            if(temp1>temp2){
                temp2=temp1;
            }
        }
        cont=temp2+1;
        
    }
    if(cont==0){
        cont=1;
    }
    equi=Equipo;
    equi=[equi stringByAppendingString:[NSString stringWithFormat: @"%ld", cont]];
    
    post= @{@"id":[NSString stringWithFormat: @"%ld", cont],
            @"idusuario": _idCliente,
            @"marca": _txtmarca.text,
            @"equipo": _txtequipo.text,
            @"modelo":_txtModelo.text,
            @"password": _txtpass.text,
            @"estado": _txtestado.text,
            @"observaciones": _txtobser.text,
            @"descripcion": _txtdesc.text};
    NSDictionary *childUpdates = @{[@"/equipos/" stringByAppendingString:equi]: post};
    [_ref updateChildValues:childUpdates];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    self.equipconController = (EquiposConsultaViewController*) viewController;
    self.equipconController.idCliente = self.idCliente;
}

@end
