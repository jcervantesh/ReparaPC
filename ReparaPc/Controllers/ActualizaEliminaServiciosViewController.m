//
//  ActualizaEliminaServiciosViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 26/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ActualizaEliminaServiciosViewController.h"
#import "StringKeys.h"

#define Servicio @"servicio"

@interface ActualizaEliminaServiciosViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *servicio;
    NSString *serv;
}

@end

@implementation ActualizaEliminaServiciosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _transparentView.hidden=YES;
    _contentView.hidden=YES;
    [self viewComponentes:_confirmarView];
    [self viewComponentes:_eliminarView];
    [self viewComponentes:_modificarView];
    _confirmarView.hidden=YES;
    
    [self SetTextFieldBorder:_txtServicio];
    [self SetTextFieldBorder:_txtCosto];
    _btnGuardar.hidden=YES;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        serv=Servicio;
        serv=[serv stringByAppendingString:_idServicio];
        servicio=[[postDict objectForKey:@"servicios"]objectForKey:serv];
    }];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)enableEdit{
    _txtServicio.enabled=YES;
    _txtCosto.enabled=YES;
    _btnGuardar.hidden=NO;
    _btnEliminar.hidden=YES;
    _btnActualizar.hidden=YES;
    _confirmarView.hidden=NO;
    _eliminarView.hidden=YES;
    _modificarView.hidden=YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ConfirmarAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ActualizarAction:(id)sender {
    [self enableEdit];
}

- (IBAction)GuardarAction:(id)sender {
    NSDictionary *post;
    post= @{@"ID":_idServicio,
            @"servicio": _txtServicio.text,
            @"costo": _txtCosto.text};
    NSDictionary *childUpdates = @{[@"/servicios/" stringByAppendingString:serv]: post};
    [_ref updateChildValues:childUpdates];
    [self confirmarActualizar];
}

-(void)confirmarActualizar{
    _transparentView.hidden=NO;
    _contentView.hidden=NO;
    _lblTextoAlert.text=alertActualizarServ;
}

- (IBAction)EliminarAction:(id)sender {
    serv=Servicio;
    serv=[serv stringByAppendingString:_idServicio];
    [[_ref child:[@"/servicios/" stringByAppendingString:serv]]removeValue];
    [self confirmarEliminar];
}

-(void)confirmarEliminar{
    _transparentView.hidden=NO;
    _contentView.hidden=NO;
    _lblTextoAlert.text=alertEliminarServ;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

-(void)viewDidAppear:(BOOL)animated{
    _txtServicio.text=[servicio objectForKey:@"servicio"];
    _txtCosto.text=[servicio objectForKey:@"costo"];
}
-(void)viewComponentes:(UIView *)myview{
    myview.layer.borderColor = [UIColor darkGrayColor].CGColor;
    myview.layer.borderWidth = 3.0f;
    myview.layer.cornerRadius = 8.0f;
}

@end
