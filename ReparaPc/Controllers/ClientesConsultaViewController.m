//
//  ClientesConsultaViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 10/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ClientesConsultaViewController.h"
#import "ClientesActualizaEliminarViewController.h"
#import "StringKeys.h"

@interface ClientesConsultaViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *clientes;
}

@end

@implementation ClientesConsultaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _tableView.layer.borderWidth = 2.0f;
    _tableView.layer.cornerRadius = 4.0f;
//    _lblTitulo.layer.borderColor = [UIColor blackColor].CGColor;
//    _lblTitulo.layer.borderWidth = 3.0f;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        clientes=[postDict objectForKey:@"usuarios"];
        [self.tableView reloadData];
        [self recalculateTable];
    }];
    _clntActEliController=[[ClientesActualizaEliminarViewController alloc]init];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [clientes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identificadorCelda=@"cell";
    UITableViewCell *celda=[tableView dequeueReusableCellWithIdentifier:identificadorCelda forIndexPath:indexPath];
    _values = [clientes allValues];
    NSDictionary *aux=[_values objectAtIndex:[indexPath row]];
    NSString *name=[aux objectForKey:@"nombre"];
    name = [name stringByAppendingString:[NSString stringWithFormat: @" %@", [aux objectForKey:@"apaterno"]]];
    NSString *mail=[aux objectForKey:@"email"];
    celda.textLabel.text=name;
    celda.detailTextLabel.text=mail;
    celda.layer.borderColor = [UIColor darkGrayColor].CGColor;
    celda.layer.borderWidth = 1.0f;
    return celda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *aux=[_values objectAtIndex:[indexPath row]];
    _clntActEliController = [[UIStoryboard storyboardWithName:STB_MENU bundle:nil] instantiateViewControllerWithIdentifier:@"IDActualizaElimina"];
    _clntActEliController.idCliente=[aux objectForKey:@"id"];
    [self.navigationController pushViewController:_clntActEliController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

-(void)recalculateTable{
    _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, ([clientes count]*50));
}
-(void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
    [self recalculateTable];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
