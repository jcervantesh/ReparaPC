//
//  ConsultaServiciosViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 26/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ConsultaServiciosViewController.h"
#import "ActualizaEliminaServiciosViewController.h"
#import "StringKeys.h"

@interface ConsultaServiciosViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *servicios;
}

@end

@implementation ConsultaServiciosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _tableView.layer.borderWidth = 2.0f;
    _tableView.layer.cornerRadius = 4.0f;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        @try {
            servicios=[postDict objectForKey:@"servicios"];
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        [self.tableView reloadData];
    }];
    _actEliServController = [[ActualizaEliminaServiciosViewController alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [servicios count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identificadorCelda=@"Cell";
    UITableViewCell *celda=[tableView dequeueReusableCellWithIdentifier:identificadorCelda forIndexPath:indexPath];
    _values = [servicios allValues];
    NSDictionary *aux=[_values objectAtIndex:[indexPath row]];
    NSString *servicio=[aux objectForKey:@"servicio"];
    NSString *costo=@"$ ";
    costo = [costo stringByAppendingString:[NSString stringWithFormat: @"%@", [aux objectForKey:@"costo"]]];
    celda.textLabel.text=servicio;
    celda.detailTextLabel.text=costo;
    celda.layer.borderColor = [UIColor darkGrayColor].CGColor;
    celda.layer.borderWidth = 1.0f;
    return celda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_idEquipo == nil){
        NSDictionary *aux=[_values objectAtIndex:[indexPath row]];
        _actEliServController = [[UIStoryboard storyboardWithName:STB_MENU bundle:nil] instantiateViewControllerWithIdentifier:@"IDActEliServicio"];
        _actEliServController.idServicio = [aux objectForKey:@"ID"];
        [self.navigationController pushViewController:_actEliServController animated:YES];
    }else{
        NSLog(@"Aqui se hara el registro cuando lo tenga");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

-(void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
    [self recalculateTable];
}
-(void)recalculateTable{
    _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, ([servicios count]*50));
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
