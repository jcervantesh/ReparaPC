//
//  ClientesConsultaViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 10/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>



@import Firebase;

@class ClientesActualizaEliminarViewController;

@interface ClientesConsultaViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) ClientesActualizaEliminarViewController *clntActEliController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@property (strong, nonatomic) NSArray * values;

- (IBAction)back:(id)sender;

@end
