//
//  EquiposConsultaViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 18/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;

@class ConsultaServiciosViewController;
@interface EquiposConsultaViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITabBarDelegate>

@property (strong, nonatomic) FIRDatabaseReference *ref;

@property (strong, nonatomic) ConsultaServiciosViewController *consServController;
@property (strong, nonatomic) NSArray * values;
@property (strong, nonatomic) NSString *idCliente;

@property (strong, nonatomic) IBOutlet UIView *optionesView;
@property (strong, nonatomic) IBOutlet UIView *transparentView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)back:(id)sender;
- (IBAction)cancelarAction:(id)sender;
- (IBAction)actualizarEliminar:(id)sender;
- (IBAction)registrarServicio:(id)sender;
- (IBAction)registrarAccesorio:(id)sender;

@end
