//
//  ClientesRegistroViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 07/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ClientesRegistroViewController.h"
#import "StringKeys.h"



@interface ClientesRegistroViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *clientes;
}

@end

@implementation ClientesRegistroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    _contentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    _contentView.layer.borderWidth = 2.0f;
//    _contentView.layer.cornerRadius = 4.0f;
    _guardar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _guardar.layer.borderWidth = 3.0f;
    _guardar.layer.cornerRadius = 8.0f;
    [self SetTextFieldBorder:_txtnombre];
    [self SetTextFieldBorder:_txtaPaterno];
    [self SetTextFieldBorder:_txtaMaterno];
    [self SetTextFieldBorder:_txttelefono];
    [self SetTextFieldBorder:_txtciudad];
    [self SetTextFieldBorder:_txteMail];
    
    
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        @try {
            clientes=[postDict objectForKey:@"usuarios"];
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }

    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (IBAction)guardarCliente:(id)sender {
    NSString *clnt;
    NSDictionary *post;
    long cont;
    long temp1=0;
    long temp2=0;
    
    cont=[clientes count];
    if(cont!=0){
        //####### Recorrer todos los registros
        NSArray * values = [clientes allValues];
        for(int i=0; i<=[values count]-1; i++){
            NSDictionary *aux=[values objectAtIndex:i];
            NSString *con=[aux objectForKey:@"id"];
            temp1=[con longLongValue];
            if(temp1>temp2){
                temp2=temp1;
            }
        }
        cont=temp2+1;
        
    }
    if(cont==0){
        cont=1;
    }
    clnt=Usuario;
    clnt=[clnt stringByAppendingString:[NSString stringWithFormat: @"%ld", cont]];
    
    post= @{@"id":[NSString stringWithFormat: @"%ld", cont],
            @"nombre": _txtnombre.text,
            @"apaterno": _txtaPaterno.text,
            @"amaterno": _txtaMaterno.text,
            @"telefono": _txttelefono.text,
            @"ciudad": _txtciudad.text,
            @"email": _txteMail.text};
    NSDictionary *childUpdates = @{[@"/usuarios/" stringByAppendingString:clnt]: post};
    
    
    
    [_ref updateChildValues:childUpdates];
    [self cleartext];
    
}
-(void)cleartext{
    _txtnombre.text=@"";
    _txtaPaterno.text=@"";
    _txtaMaterno.text=@"";
    _txttelefono.text=@"";
    _txtciudad.text=@"";
    _txteMail.text=@"";
}
@end
