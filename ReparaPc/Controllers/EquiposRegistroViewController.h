//
//  EquiposRegistroViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 18/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;
@class EquiposConsultaViewController;

@interface EquiposRegistroViewController : UIViewController <UITabBarControllerDelegate>

@property (strong, nonatomic) NSString *idCliente;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) EquiposConsultaViewController *equipconController;

@property (strong, nonatomic) IBOutlet UITextField *txtmarca;
@property (strong, nonatomic) IBOutlet UITextField *txtequipo;
@property (strong, nonatomic) IBOutlet UITextField *txtpass;
@property (strong, nonatomic) IBOutlet UITextField *txtestado;
@property (strong, nonatomic) IBOutlet UITextField *txtobser;
@property (strong, nonatomic) IBOutlet UITextField *txtdesc;
@property (strong, nonatomic) IBOutlet UITextField *txtModelo;
@property (strong, nonatomic) IBOutlet UIButton *btnGuardar;

- (IBAction)Guardar:(id)sender;

- (IBAction)back:(id)sender;

@end
