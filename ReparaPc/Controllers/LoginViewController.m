//
//  LoginViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 26/04/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "LoginViewController.h"
#import "RPFloatingPlaceholderTextField.h"
#import "RPFloatingPlaceholderTextView.h"
#import "CarruselViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "StringKeys.h"




@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _indicador.hidden=YES;
    self.navigationController.navigationBarHidden=YES;
    _userName.leftViewMode = UITextFieldViewModeAlways;
    _userName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user"]];
    _password.leftViewMode=UITextFieldViewModeAlways;
    _password.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pass"]];
    [self SetTextFieldBorder:_userName];
    [self SetTextFieldBorder:_password];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showMenuPrincipal:(id)sender {
    [self startActivityIndicator];
    [self performSelector:@selector(MostrarStory) withObject:nil afterDelay:5.0];
    //[self MostrarStory];
}


-(void)viewWillAppear:(BOOL)animated{
    [self setComponentes];
}

-(void)setComponentes{
    self.navigationController.navigationBarHidden=YES;
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}
-(void)MostrarStory{
    _carruselController = [[UIStoryboard storyboardWithName:STB_MENU bundle:nil] instantiateViewControllerWithIdentifier:@"MenuID"];
    [self.navigationController pushViewController:_carruselController animated:YES];
    [self stopActivityIndicator];
}

- (void)startActivityIndicator {
    _indicador.hidden=NO;
    [_indicador startAnimating];
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

-(void)stopActivityIndicator{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [_indicador removeFromSuperview];
    [_indicador stopAnimating];
}
@end
