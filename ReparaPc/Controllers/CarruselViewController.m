//
//  CarruselViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 03/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "CarruselViewController.h"
#import "SWRevealViewController.h"
#import "ServiciosViewController.h"
#import "ClientesEquiposViewController.h"
#import "StringKeys.h"


#define _AUTO_SCROLL_ENABLED 0

@interface CarruselViewController (){
    NSMutableArray *imagesArray;
    SBSliderView *slider;
    FIRDatabaseHandle _refHandle;
    NSDictionary *imagenes;
    
}

@end

@implementation CarruselViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _activityIndicador.hidden=YES;
    [self startActivityIndicator];
    [self viewComponentes:_clientesView];
    [self viewComponentes:_serviciosView];
    [self viewComponentes:_ordenesView];
    [self viewComponentes:_equiposView];
    _img = YES;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        imagenes=[postDict objectForKey:@"imagenes"];
        if(_img){
            [self setCarrusel];
        }
        
    }];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor lightGrayColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];

}
- (void)startActivityIndicator {
    _activityIndicador.hidden=NO;
    [_activityIndicador startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

-(void)stopActivityIndicator{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [_activityIndicador removeFromSuperview];
    [_activityIndicador stopAnimating];
}

-(void)setCarrusel{
    _img = NO;
    NSArray *aux = [imagenes allValues];
    imagesArray = [[NSMutableArray alloc] init];
    for(int i=0;i<[aux count];i++){
        [imagesArray addObject:[aux objectAtIndex:i]];
    }
    slider = [[[NSBundle mainBundle] loadNibNamed:@"SBSliderView" owner:self options:nil] firstObject];
    slider.delegate = self;
    [slider createSliderWithImages:imagesArray WithAutoScroll:_AUTO_SCROLL_ENABLED inView:self.view];
    slider.frame = CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width, slider.frame.size.height);
    //_autoPlayToggle.on = _AUTO_SCROLL_ENABLED;
    [self.view addSubview:slider];
    [_preimg removeFromSuperview];
    [self stopActivityIndicator];
}

-(void)viewComponentes:(UIView *)myview{
    myview.layer.borderColor = [UIColor darkGrayColor].CGColor;
    myview.layer.borderWidth = 3.0f;
    myview.layer.cornerRadius = 8.0f;
}


- (void)sbslider:(SBSliderView *)sbslider didTapOnImage:(UIImage *)targetImage andParentView:(UIImageView *)targetView {
    
    SBPhotoManager *photoViewerManager = [[SBPhotoManager alloc] init];
    [photoViewerManager initializePhotoViewerFromViewControlller:self forTargetImageView:targetView withPosition:sbslider.frame];
}

- (IBAction)toggleAutoPlay:(id)sender {
    
    UISwitch *toggleSwitch = (UISwitch *)sender;
    
    if ([toggleSwitch isOn]) {
        [slider startAutoPlay];
    } else {
        [slider stopAutoPlay];
    }
}

- (IBAction)tappedOnSampleImage:(id)sender {
    
    UIGestureRecognizer *gesture = (UIGestureRecognizer *)sender;
    UIImageView *targetView = (UIImageView *)gesture.view;
    
    SBPhotoManager *photoViewerManager = [[SBPhotoManager alloc] init];
    [photoViewerManager initializePhotoViewerFromViewControlller:self forTargetImageView:targetView withPosition:targetView.frame];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showServicios:(id)sender {
}

- (IBAction)showEquipos:(id)sender {
    _equiposController = [[UIStoryboard storyboardWithName:STB_CLIENTES bundle:nil] instantiateViewControllerWithIdentifier:@"IDClientesEquipos"];
    [self.navigationController pushViewController:_equiposController animated:YES];
}
@end
