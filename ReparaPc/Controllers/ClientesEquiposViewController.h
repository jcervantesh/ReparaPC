//
//  ClientesEquiposViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 14/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>


@import Firebase;

@class EquiposRegistroViewController;

@class TabBarController;

@interface ClientesEquiposViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) EquiposRegistroViewController *equipregController;

@property (strong, nonatomic) TabBarController *tabController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray * values;

- (IBAction)back:(id)sender;
@end
