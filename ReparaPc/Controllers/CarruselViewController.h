//
//  CarruselViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 03/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBSliderView.h"
#import "SBPhotoManager.h"

@import Firebase;
@class ServiciosViewController;
@class ClientesEquiposViewController;

@interface CarruselViewController : UIViewController<SBSliderDelegate>

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (nonatomic) IBOutlet UIButton *revealButtonItem;
@property (strong, nonatomic) IBOutlet UIView *clientesView;
@property (strong, nonatomic) IBOutlet UIView *serviciosView;
@property (strong, nonatomic) IBOutlet UIView *equiposView;
@property (strong, nonatomic) IBOutlet UIView *ordenesView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicador;
@property (strong, nonatomic) IBOutlet UIImageView *preimg;

@property (nonatomic) BOOL img;



@property(nonatomic,strong)ServiciosViewController *servicioController;

@property(nonatomic,strong)ClientesEquiposViewController *equiposController;



- (IBAction)showServicios:(id)sender;

- (IBAction)showEquipos:(id)sender;



@end
