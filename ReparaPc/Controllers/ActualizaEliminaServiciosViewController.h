//
//  ActualizaEliminaServiciosViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 26/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@interface ActualizaEliminaServiciosViewController: UIViewController

@property (strong, nonatomic) FIRDatabaseReference *ref;

@property (strong, nonatomic) NSString *idServicio;

@property (strong, nonatomic) IBOutlet UITextField *txtServicio;
@property (strong, nonatomic) IBOutlet UITextField *txtCosto;
@property (strong, nonatomic) IBOutlet UIButton *btnActualizar;
@property (strong, nonatomic) IBOutlet UIButton *btnGuardar;
@property (strong, nonatomic) IBOutlet UIButton *btnEliminar;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *lblTextoAlert;
@property (strong, nonatomic) IBOutlet UIView *transparentView;
@property (strong, nonatomic) IBOutlet UIView *modificarView;
@property (strong, nonatomic) IBOutlet UIView *eliminarView;
@property (strong, nonatomic) IBOutlet UIView *confirmarView;

- (IBAction)ConfirmarAction:(id)sender;
- (IBAction)ActualizarAction:(id)sender;
- (IBAction)GuardarAction:(id)sender;
- (IBAction)EliminarAction:(id)sender;
- (IBAction)back:(id)sender;




@end
