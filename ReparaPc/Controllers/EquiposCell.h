//
//  EquiposCell.h
//  ReparaPc
//
//  Created by Javier Cervantes on 22/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquiposCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMarca;
@property (strong, nonatomic) IBOutlet UITextView *lblDescrip;
@property (strong, nonatomic) IBOutlet UILabel *lblPass;
@property (strong, nonatomic) IBOutlet UILabel *lblEquipo;
@property (strong, nonatomic) IBOutlet UIImageView *imgEquipo;


@end
