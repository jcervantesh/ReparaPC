//
//  EquiposConsultaViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 18/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "EquiposConsultaViewController.h"
#import "EquiposCell.h"
#import "ConsultaServiciosViewController.h"
#import "StringKeys.h"

@interface EquiposConsultaViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *equipos;
    NSMutableArray *equiposUsuario;
    NSDictionary *aux;
}

@end

@implementation EquiposConsultaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _tableView.layer.borderWidth = 2.0f;
    _tableView.layer.cornerRadius = 4.0f;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        equiposUsuario = [[NSMutableArray alloc]init];
        NSDictionary *postDict = snapshot.value;
        equipos=[postDict objectForKey:@"equipos"];
        _values = [equipos allValues];
        for(int i=0;i<[_values count];i++){
            NSDictionary *aux1=[_values objectAtIndex:i];
            if([[aux1 objectForKey:@"idusuario"]isEqualToString:_idCliente]){
                NSMutableArray *eUsuario = [[NSMutableArray alloc]init];
                [eUsuario addObjectsFromArray:[aux1 allValues]];
                [equiposUsuario addObject: eUsuario];
                
            }
        }
        [self.tableView reloadData];
    }];
    _consServController = [[ConsultaServiciosViewController alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [equiposUsuario count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identificadorCelda=@"cellEquipos";
    
    EquiposCell *celda= (EquiposCell *)[tableView dequeueReusableCellWithIdentifier:identificadorCelda];
    if (celda == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EquipoCellView" owner:self options:nil];
        celda = [topLevelObjects objectAtIndex:0];
        celda.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    NSString *equipo =  [[equiposUsuario objectAtIndex:[indexPath row]]objectAtIndex:0];
    if([equipo isEqualToString:@"Consola"]){
        celda.imgEquipo.image = [UIImage imageNamed:@"consola"];
    }else if([equipo isEqualToString:@"Laptop"]){
        celda.imgEquipo.image = [UIImage imageNamed:@"lap"];
    }else if([equipo isEqualToString:@"PC"]){
        celda.imgEquipo.image = [UIImage imageNamed:@"pc"];
    }else if([equipo isEqualToString:@"Tablet"]){
        celda.imgEquipo.image = [UIImage imageNamed:@"tablet"];
    }else if([equipo isEqualToString:@"Celular"]){
        celda.imgEquipo.image = [UIImage imageNamed:@"cel"];
    }else{
        celda.imgEquipo.image = [UIImage imageNamed:@"equipos"];
    }
    
    celda.lblMarca.text =  [[equiposUsuario objectAtIndex:[indexPath row]]objectAtIndex:5];
    celda.lblMarca.backgroundColor = [UIColor clearColor];
    celda.lblMarca.font=[UIFont fontWithName:@"Helvetica Neue" size:27];
    celda.lblMarca.textColor=[UIColor blackColor];
    
    celda.lblPass.text =  [[equiposUsuario objectAtIndex:[indexPath row]]objectAtIndex:7];
    celda.lblPass.backgroundColor = [UIColor clearColor];
    celda.lblPass.font=[UIFont fontWithName:@"Helvetica Neue" size:14];
    celda.lblPass.textColor=[UIColor darkGrayColor];
    
    celda.lblDescrip.text =  [[equiposUsuario objectAtIndex:[indexPath row]]objectAtIndex:2];
    celda.lblDescrip.backgroundColor = [UIColor clearColor];
    celda.lblDescrip.font=[UIFont fontWithName:@"Helvetica Neue" size:15];
    celda.lblDescrip.textColor=[UIColor darkGrayColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    celda.layer.borderColor = [UIColor darkGrayColor].CGColor;
    celda.layer.borderWidth = 1.0f;
    return celda;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145.0f;
}

-(void)recalculateTable{
    if([equiposUsuario count]<3){
         _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, ([equiposUsuario count]*145.0f));
    }
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    aux=[_values objectAtIndex:[indexPath row]];
    
    [self showOptions];

}

-(void)showOptions{
    _optionesView.hidden=NO;
    _transparentView.hidden=NO;
    [self.tabBarController.view addSubview:_transparentView];
    [self.tabBarController.view addSubview:_optionesView];
}

-(void)showRegistroServicio{
    [self cancelarAction];
    _consServController = [[UIStoryboard storyboardWithName:STB_MENU bundle:nil] instantiateViewControllerWithIdentifier:@"IDConsultaServicios"];
    _consServController.idEquipo = [aux objectForKey:@"id"];
    [self.navigationController pushViewController:_consServController animated:YES];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelarAction{
    _optionesView.hidden=YES;
    _transparentView.hidden=YES;
    [_transparentView removeFromSuperview];
    [_optionesView removeFromSuperview];
}

- (IBAction)cancelarAction:(id)sender {
    [self cancelarAction];
}

- (IBAction)actualizarEliminar:(id)sender {
}

- (IBAction)registrarServicio:(id)sender {
    [self showRegistroServicio];
}

- (IBAction)registrarAccesorio:(id)sender {
}

-(void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
    [self recalculateTable];
}

@end
