//
//  LoginViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 26/04/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuPrincipalViewController;
@class CarruselViewController;

@interface LoginViewController : UIViewController
- (IBAction)showMenuPrincipal:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicador;

@property(nonatomic,strong)CarruselViewController *carruselController;

@end
