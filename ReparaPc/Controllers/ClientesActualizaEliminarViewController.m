//
//  ClientesActualizaEliminarViewController.m
//  ReparaPc
//
//  Created by Javier Cervantes on 10/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import "ClientesActualizaEliminarViewController.h"
#import "ClientesConsultaViewController.h"
#import "StringKeys.h"

#define Usuario @"usuario"

@interface ClientesActualizaEliminarViewController (){
    FIRDatabaseHandle _refHandle;
    NSDictionary *clientes;
    NSString *clnt;
}

@end

@implementation ClientesActualizaEliminarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _transparentView.hidden=YES;
    _datosView.hidden=YES;
    [self viewComponentes:_actualizarView];
    [self viewComponentes:_eliminarView];
    [self viewComponentes:_confirmarView];
    [self SetTextFieldBorder:_txtNombre];
    [self SetTextFieldBorder:_txtaPaterno];
    [self SetTextFieldBorder:_txtaMaterno];
    [self SetTextFieldBorder:_txtTelefono];
    [self SetTextFieldBorder:_txtCiudad];
    [self SetTextFieldBorder:_txtEmail];
    _confirmarView.hidden=YES;
    _ref = [[FIRDatabase database] reference];
    _refHandle = [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *postDict = snapshot.value;
        clnt=Usuario;
        clnt=[clnt stringByAppendingString:_idCliente];
        clientes=[[postDict objectForKey:@"usuarios"]objectForKey:clnt];
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewComponentes:(UIView *)myview{
    myview.layer.borderColor = [UIColor darkGrayColor].CGColor;
    myview.layer.borderWidth = 3.0f;
    myview.layer.cornerRadius = 8.0f;
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

-(void)enableEdit{
    _txtNombre.enabled=YES;
    _txtaPaterno.enabled=YES;
    _txtaMaterno.enabled=YES;
    _txtTelefono.enabled=YES;
    _txtCiudad.enabled=YES;
    _txtEmail.enabled=YES;
    _confirmarView.hidden=NO;
    _eliminarView.hidden=YES;
    _actualizarView.hidden=YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actualizar:(id)sender {
    [self enableEdit];
}

- (IBAction)eliminar:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Eliminar" message:@"¿Seguro que quiere eliminar el usuario?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"OK",nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex==0){
        NSLog(@"NO");
    }else{
        clnt=Usuario;
        clnt=[clnt stringByAppendingString:_idCliente];
        [[_ref child:[@"/usuarios/" stringByAppendingString:clnt]]removeValue];
        [self confirmarEliminar];
    }
}


-(void)confirmarEliminar{
    _transparentView.hidden=NO;
    _datosView.hidden=NO;
    _lblTextoAlerta.text=alertEliminar;
}

- (IBAction)confirmar:(id)sender {
    NSDictionary *post;
    post= @{@"id":_idCliente,
            @"nombre": _txtNombre.text,
            @"apaterno": _txtaPaterno.text,
            @"amaterno": _txtaMaterno.text,
            @"telefono": _txtTelefono.text,
            @"ciudad": _txtCiudad.text,
            @"email": _txtEmail.text};
    NSDictionary *childUpdates = @{[@"/usuarios/" stringByAppendingString:clnt]: post};
    [_ref updateChildValues:childUpdates];
    [self confirmarActualizar];
}
-(void)confirmarActualizar{
    _transparentView.hidden=NO;
    _datosView.hidden=NO;
    _lblTextoAlerta.text=alertActualizar;
}

- (IBAction)alertaccion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    _txtNombre.text=[clientes objectForKey:@"nombre"];
    _txtaPaterno.text=[clientes objectForKey:@"apaterno"];
    _txtaMaterno.text=[clientes objectForKey:@"amaterno"];
    _txtTelefono.text=[clientes objectForKey:@"telefono"];
    _txtCiudad.text=[clientes objectForKey:@"ciudad"];
    _txtEmail.text=[clientes objectForKey:@"email"];
}
@end
