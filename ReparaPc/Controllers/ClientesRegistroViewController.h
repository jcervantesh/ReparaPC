//
//  ClientesRegistroViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 07/05/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;

@interface ClientesRegistroViewController : UIViewController
- (IBAction)back:(id)sender;

@property (strong, nonatomic) FIRDatabaseReference *ref;

@property (strong, nonatomic) IBOutlet UIButton *guardar;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITextField *txtnombre;
@property (strong, nonatomic) IBOutlet UITextField *txtaPaterno;
@property (strong, nonatomic) IBOutlet UITextField *txtaMaterno;
@property (strong, nonatomic) IBOutlet UITextField *txttelefono;
@property (strong, nonatomic) IBOutlet UITextField *txteMail;
@property (strong, nonatomic) IBOutlet UITextField *txtciudad;

- (IBAction)guardarCliente:(id)sender;
@end
