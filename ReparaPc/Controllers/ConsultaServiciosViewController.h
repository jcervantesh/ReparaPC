//
//  ConsultaServiciosViewController.h
//  ReparaPc
//
//  Created by Javier Cervantes on 26/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase;

@class ActualizaEliminaServiciosViewController;

@interface ConsultaServiciosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) ActualizaEliminaServiciosViewController *actEliServController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray * values;
@property (strong, nonatomic) NSString *idEquipo;
- (IBAction)back:(id)sender;

@end
