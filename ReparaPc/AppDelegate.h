//
//  AppDelegate.h
//  ReparaPc
//
//  Created by Beca IDS on 27/02/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LoginViewController.h"
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property(strong,nonatomic)LoginViewController *loginController;

- (void)saveContext;


@end

