//
//  StringKeys.h
//  ReparaPc
//
//  Created by Javier Cervantes on 18/06/17.
//  Copyright © 2017 Beca IDS. All rights reserved.
//

#ifndef StringKeys_h
#define StringKeys_h


#define STB_MENU @"MenuView"
#define STB_CLIENTES @"ClientesViewStoryboard"
#define MENUP_XIB @"MenuPrincipalView"

#define alertEliminarServ @"Servicio Eliminado Exitosamente"
#define alertActualizarServ @"Servicio Actualizado Exitosamente"

#define alertEliminar @"Usuario Eliminado Exitosamente"
#define alertActualizar @"Usuario Actualizado Exitosamente"

#define Usuario @"usuario"
#define Equipo @"equipo"


#endif /* StringKeys_h */
